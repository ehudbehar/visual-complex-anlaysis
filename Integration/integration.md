# Complex Integration

Visualize complex integration with GeoGebra
## Bézier curve
The contour to be integrated is a Bézier curve.

Define 5 control points with the name $`p_0,\ldots,p_4`$, and the parametric curve $`a`$:

`a=Curve((1-t)^(4)x(p_{0})+4(1-t)^(3)t*x(p_{1})+6(1-t)^(2)t^(2)x(p_{2})+4 (1-t)t^(3)x(p_{3})+t^(4)x(p_{4}),(1-t)^(4)y(p_{0})+4(1-t)^(3)t*y(p_{1})+6(1-t)^(2)t^(2)y(p_{2})+4 (1-t)t^(3)y(p_{3})+t^(4)y(p_{4}),t,0,1)`.

Use a slider $`N\in \mathbb{Z}`$ for the number of points.

1.  We need to make a list of points that break up the contour (These are the white hollow points in Needham's Fig[7] page 384): If you really think of it no use is ever being made with this list in the computation of the integral. After you get all the lists correctly it is cleaner not to define it so the graphics view is less cluttered.

2. Points in the middle of each vector (Black points in the above mentioned figure):
`z_{i}=Sequence(x(Point(a,1-(n)/(N)+(1)/(2 N)))+ί y(Point(a,1-(n)/(N)+(1)/(2 N))),n,1,N)`
3.  A list of short vectors along the contour,
$`\Delta_i`$: `Δ_{i}=Sequence(Vector(x(Point(a, (n - 1) / N)) + ί y(Point(a, (n - 1) / N)), x(Point(a, n / N)) + ί y(Point(a, n / N))), n, 1, N)`.

4.  $`f(z_i)`$, Image of $`z_i`$ in the $`w`$-plane: Define a function $`f(z)`$ in the usual way of typing it in the algebra view or the command line. Create the list `w_{i}=f(z_{i})`.

5.  $`f(z_i)\Delta z_i`$.
This List shouldn't be visible too. One should see on the Graphics 2 view only the list of points $`w_i`$s that were mapped to the $`w`$ plane from the $`z`$ plane. Recall that $`(x + i y) (a + i b) = a x - b y + i (b x + a y)`$. `R_{m}=Sequence(x(Element(w_{i}, k)) x(Element(Δ_{i}, k)) - y(Element(w_{i}, k)) y(Element(Δ_{i}, k)) + ί (y(Element(w_{i}, k)) x(Element(Δ_{i}, k)) + x(Element(w_{i}, k)) y(Element(Δ_{i}, k))), k, 1, N)` together with `R=Vector(Sum(R_{m})` or simply `R_{m}=Vector(Sum(Sequence(x(Element(w_{i},k)) x(Element(Δ_{i},k))-y(Element(w_{i},k)) y(Element(Δ_{i},k))+ί (y(Element(w_{i},k)) x(Element(Δ_{i},k))+x(Element(w_{i},k)) y(Element(Δ_{i},k))),k,1,N)))`.
7.  Done!

A few more things to do:
- Correct the list $`\Delta_i$`$ without the use of reverse.
- Create a tip-to-tail vector addition that shows how the vector $`R_m`$ is created.
- Add a toggle to select wheteher the curve $`a`$ is a circle (closed contour) or an open contour.

A neater way to create points along the curve:
 - `T = Sequence(t, t, 0, 1, 1 / (N - 1))` with `N` is an integer running from 1 to desired number of points.
 - `Sequence(Point(a, (Element(T, i) + Element(T, i + 1)) / 2), i, 0, N)`